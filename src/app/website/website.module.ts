import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WebsiteRoutingModule } from './website-routing.module';
import { LayoutComponent } from './layout/layout.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ManagementItemsModule } from './management-items/management-items.module';
import { NotFoundModule } from './not-found/not-found.module';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatDividerModule} from "@angular/material/divider";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    LayoutComponent,
    NotFoundComponent
  ],
  imports: [
    CommonModule,
    WebsiteRoutingModule,
    ManagementItemsModule,
    NotFoundModule,
    MatToolbarModule,
    MatSidenavModule,
    MatDividerModule,
    MatIconModule,
    MatButtonModule
  ]
})
export class WebsiteModule { }
