import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManagementItemsComponent } from './management-items.component';
import { ManagementItemsRoutingModule } from './management-items-routing.module';



@NgModule({
  declarations: [
    ManagementItemsComponent
  ],
  imports: [
    CommonModule,
    ManagementItemsRoutingModule
  ]
})
export class ManagementItemsModule { }
