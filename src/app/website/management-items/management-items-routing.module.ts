import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ManagementItemsComponent} from "./management-items.component";

const routes: Routes = [
  {
    path: '',
    component: ManagementItemsComponent
  }
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ManagementItemsRoutingModule { }
